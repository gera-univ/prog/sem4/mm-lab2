from mcg import mcg
import math
import matplotlib.pyplot as plt

def binomial_gen(n, p, random_generator):
    for r in random_generator:
        x = 0
        for i in range(n):
            if r < p:
                x += 1
        yield x

def bernoulli_gen(p, random_generator):
    yield from binomial_gen(1, p, random_generator)


if __name__ == "__main__":
    g = bernoulli_gen(0.75, mcg(2**31, 16807, 16807, count=1000))
    seq = [n for n in g]
    plt.plot(seq)
    plt.show()
    print(seq)

