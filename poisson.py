from mcg import mcg
import math
import matplotlib.pyplot as plt

def poisson_gen(mu, random_generator):
    for r in random_generator:
        p = math.exp(-mu)
        a = 1.0
        x = 0

        while True:
            x += 1
            a *= r
            if a <= p:
                break

        yield x - 1


if __name__ == "__main__":
    g = poisson_gen(0.7, mcg(2**31, 16807, 16807, count=1000))
    seq = [n for n in g]
    plt.plot(seq)
    plt.show()

